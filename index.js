const express = require("express");
const FoodStore = require('./models/FoodStore');
const path = require('path');
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/products', (req, res) => {
  const store =  new FoodStore;

  res.send(store.menu);
});

app.post('/calculate', (req, res) => {
  const order = req.body;
  const store = new FoodStore(order.member);
  const price = store.calculatePrice(order.items);
  res.send({ total: price });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
