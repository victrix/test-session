class FoodStore {
  constructor(member = false) {
    this.menu = {
      red: 50,
      green: 40,
      blue: 30,
      yellow: 50,
      pink: 80,
      purple: 90,
      orange: 120,
    };
    this.member = member;
  }

  calculatePrice(order) {
    let total = 0;

    for (const item in order) {
      const price = this.menu[item] || 0;
      const quantity = order[item];
      total += price * quantity;

      if (quantity >= 2 && (item === "orange" || item === "pink" || item === "green")) {
        total -= total * 0.05;
      }
    }

    if (this.member) {
      total -= total * 0.1;
    }

    return total;
  }
}

module.exports = FoodStore;
