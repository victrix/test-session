const FoodStore = require("../models/FoodStore");

test("Desk#1: Customers order Red set and Green set; price from calculation is 90", () => {
  const store = new FoodStore();
  const price = store.calculatePrice({ red: 1, green: 1 });
  expect(price).toBe(90);
});

test("Desk#2: Customer with a 10% discount card orders Red set and Green set; price should be 81", () => {
  const store = new FoodStore(true);
  const price = store.calculatePrice({ red: 1, green: 1 });
  expect(price).toBe(81);
});

test("Desk#3: Customers order more than 2 Orange sets; price should have a 5% discount", () => {
  const store = new FoodStore();
  const price = store.calculatePrice({ orange: 3 });
  expect(price).toBe(342);
});
